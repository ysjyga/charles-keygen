# charles-keygen

## 使用方法

### 实例化

```java
CharlesKeygen keygen = new CharlesKeygen();
keygen.keygen("Allen");
```

### 直接使用工具方法

```java
KeygenUtil.keygen("Allen");
```

