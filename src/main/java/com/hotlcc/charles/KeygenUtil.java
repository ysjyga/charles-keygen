package com.hotlcc.charles;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Charles 注册机 util
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class KeygenUtil {
    private final static CharlesKeygen CHARLES_KEYGEN = new CharlesKeygen();

    public static String keygen(String name) {
        if (name == null || name.isEmpty()) {
            throw new RuntimeException("Parameter 'name' cannot empty.");
        }
        return CHARLES_KEYGEN.keygen(name);
    }
}
